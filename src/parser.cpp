#include <parser.hpp>

void Parser::load(std::vector<Token> tokens) {
  this->tokens = tokens;
  this->pos = 0;
}

Token Parser::peek(size_t n) {
  if (pos + n >= tokens.size())
    return Token(Token::Kind::End);
  else
    return tokens[pos + n];
}

void Parser::advance(size_t n) { pos += n; }

bool Parser::parse_context(Context &ctx) {
  while (true) {
    if (peek().kind == Token::Kind::End)
      return true;
    if (peek().kind == Token::Kind::TypeVariable) {
      TypeVariable tv;
      tv.name = peek().symbol;
      if (ctx.domain.count(tv))
        return false;
      ctx.domain.insert(tv);
      advance(1);
      if (peek().kind != Token::Kind::Colon)
        return false;
      advance(1);
      if (peek().kind != Token::Kind::Star)
        return false;
      advance(1);
      if (peek().kind == Token::Kind::Comma)
        advance(1);
    } else if (peek().kind == Token::Kind::ObjectVariable) {
      ObjectVariable ov;
      ov.name = peek().symbol;
      if (ctx.domain.count(ov))
        return false;
      ctx.domain.insert(ov);
      advance(1);
      if (peek().kind != Token::Kind::Colon)
        return false;
      advance(1);
      Type t;
      if (!parse_type(t))
        return false;
      if (!type_context_check(t, ctx))
        return false;
      ctx.domain.insert(ov);
      ctx.type[ov] = t;
      if (peek().kind == Token::Kind::Comma)
        advance(1);
    } else
      return false;
  }
}

// Проверка контекста происходит в парсере, потому что к контексту есть
// изначальные требования корректности.
bool Parser::type_context_check(const Type &t, const Context &ctx) {
  if (std::holds_alternative<TypeVariable *>(t)) {
    TypeVariable *tv = std::get<TypeVariable *>(t);
    if (!bound.count(*tv) && !ctx.domain.count(*tv))
      return false;
    else
      return true;
  } else if (std::holds_alternative<TypeFunctional *>(t)) {
    TypeFunctional *tf = std::get<TypeFunctional *>(t);
    return type_context_check(tf->from, ctx) && type_context_check(tf->to, ctx);
  } else {
    TypeBinder *tb = std::get<TypeBinder *>(t);
    if (ctx.domain.count(*(tb->v)) || bound.count(*(tb->v)))
      return false;
    bound.insert(*(tb->v));
    bool result = type_context_check(tb->type, ctx);
    bound.erase(*(tb->v));
    return result;
  }
}

bool Parser::parse_type_variable(Type &t) {
  if (peek().kind == Token::Kind::TypeVariable) {
    auto v = new TypeVariable;
    v->name = peek().symbol;
    advance(1);
    t = v;
    return true;
  } else
    return false;
}

// Разрешаем нотацию следующего вида Π α, β : ∗ . α → β → α
// Аналогично с абстракциями.
bool Parser::parse_type_binder(Type &t) {
  if (peek().kind != Token::Kind::Exmark)
    return false;
  advance(1);
  std::vector<TypeVariable *> variables;
  while (true) {
    if (peek().kind != Token::Kind::TypeVariable)
      return false;
    Type v;
    if (!parse_type_variable(v))
      return false;
    TypeVariable *tv = std::get<TypeVariable *>(v);
    variables.push_back(tv);
    if (peek().kind == Token::Kind::Comma) {
      advance(1);
      continue;
    }
    if (peek().kind == Token::Kind::Colon) {
      advance(1);
      break;
    }
  }

  if (peek().kind != Token::Kind::Star)
    return false;
  advance(1);
  if (peek().kind != Token::Kind::Dot)
    return false;
  advance(1);
  Type type;
  if (!parse_type(type))
    return false;

  TypeBinder *ret = new TypeBinder;
  ret->v = variables[variables.size() - 1];
  ret->type = type;
  for (int i = variables.size() - 2; i >= 0; i--) {
    TypeBinder *tb_i = new TypeBinder;
    tb_i->v = variables[i];
    tb_i->type = ret;
    ret = tb_i;
  }

  t = ret;
  return true;
}

// Правая ассоциативность, парсим рекурсивно.
bool Parser::parse_type(Type &t) {
  Type t1, t2;
  switch (peek().kind) {
  case Token::Kind::LParen: {
    parens++;
    type_parens++;
    advance(1);
    if (!parse_type(t1))
      return false;
    if (peek().kind != Token::Kind::RParen)
      return false;
    parens--;
    type_parens--;
    advance(1);
    break;
  }
  case Token::Kind::TypeVariable: {
    if (!parse_type_variable(t1))
      return false;
    break;
  }
  case Token::Kind::Exmark: {
    if (!parse_type_binder(t1))
      return false;
    break;
  }
  default:
    return false;
  }

  switch (peek().kind) {
  case Token::Kind::RParen: {
    if (parens == 0)
      return false;
    t = t1;
    return true;
  }
  case Token::Kind::Arrow: {
    advance(1);
    if (!parse_type(t2))
      return false;
    break;
  }
  default:
    if (type_parens != 0)
      return false;
    t = t1;
    return true;
  }

  TypeFunctional *ret = new TypeFunctional;
  ret->from = t1;
  ret->to = t2;
  t = ret;
  return true;
}

bool Parser::parse_object_variable(Term &t) {
  if (peek().kind == Token::Kind::ObjectVariable) {
    auto v = new ObjectVariable;
    v->name = peek().symbol;
    advance(1);
    t = v;
    return true;
  } else
    return false;
}

bool Parser::parse_abstraction(Term &t) {
  if (peek().kind != Token::Kind::BSlash)
    return false;
  advance(1);
  std::vector<Term> variables;
  while (true) {
    if (peek().kind != Token::Kind::ObjectVariable)
      return false;
    Term tv;
    if (!parse_object_variable(tv))
      return false;
    variables.push_back(tv);
    if (peek().kind == Token::Kind::Comma) {
      advance(1);
      continue;
    }
    if (peek().kind == Token::Kind::Colon) {
      advance(1);
      break;
    }
  }
  Type type;
  if (!parse_type(type))
    return false;
  if (peek().kind != Token::Kind::Dot)
    return false;
  advance(1);
  Term term;
  if (!parse_term(term))
    return false;

  auto ret = new Abstraction;
  ret->term = term;
  ret->type = type;
  ret->v = std::get<ObjectVariable *>(variables[variables.size() - 1]);
  for (int i = variables.size() - 2; i >= 0; i--) {
    Abstraction *a_i = new Abstraction;
    a_i->term = ret;
    a_i->type = type;
    a_i->v = std::get<ObjectVariable *>(variables[i]);
    ret = a_i;
  }
  t = ret;
  return true;
}

bool Parser::parse_type_abstraction(Term &t) {
  if (peek().kind != Token::Kind::BSlash)
    return false;
  advance(1);
  std::vector<TypeVariable *> variables;
  while (true) {
    if (peek().kind != Token::Kind::TypeVariable)
      return false;
    Type v;
    if (!parse_type_variable(v))
      return false;
    TypeVariable *tv = std::get<TypeVariable *>(v);
    variables.push_back(tv);
    if (peek().kind == Token::Kind::Comma) {
      advance(1);
      continue;
    }
    if (peek().kind == Token::Kind::Colon) {
      advance(1);
      break;
    }
  }

  if (peek().kind != Token::Kind::Star)
    return false;
  advance(1);
  if (peek().kind != Token::Kind::Dot)
    return false;
  advance(1);
  Term term;
  if (!parse_term(term))
    return false;

  auto ret = new TypeAbstraction;
  ret->v = variables[variables.size() - 1];
  ret->term = term;
  for (int i = variables.size() - 2; i >= 0; i--) {
    TypeAbstraction *a_i = new TypeAbstraction;
    a_i->term = ret;
    a_i->v = variables[i];
    ret = a_i;
  }
  t = ret;
  return true;
}

// Так как могут быть выражения типа Λ2(T2), где (T2) парсится рекурсивно
// Term и Type были объединены в один тип.
// Λ2 не может иметь вид T2. Эта функция проверяет, что данный Op действительно
// Term, то есть имеет вид Λ2 = V|(Λ2 Λ2)|(Λ2 T2)|(λV : T2 . Λ2)|(λV : ∗ . Λ2).
bool Parser::parse_term(Term &t) {
  Op op;
  if (!parse_term_internal(op))
    return false;
  if (std::holds_alternative<Type>(op))
    return false;
  t = std::get<Term>(op);
  return true;
}

// Левая ассоциативность, парсим с помощью цикла.
bool Parser::parse_term_internal(Op &t) {
  std::vector<Op> ops;

  while (true) {
    Term term;
    Type type;
    if (peek().kind == Token::Kind::End) {
      if (parens != 0)
        return false;
      if (ops.empty())
        return false;
      break;
    }
    if (peek().kind == Token::Kind::RParen) {
      if (parens == 0)
        return false;
      if (ops.empty())
        return false;
      break;
    }

    size_t cur_pos = pos;
    int cur_parens = parens;
    int cur_type_parens = type_parens;

    if (parse_type(type)) {
      ops.push_back(type);
      continue;
    }

    pos = cur_pos;
    parens = cur_parens;
    type_parens = cur_type_parens;

    if (peek().kind == Token::Kind::LParen) {
      parens++;
      advance(1);
      if (!parse_term(term))
        return false;
      if (peek().kind != Token::Kind::RParen)
        return false;
      parens--;
      advance(1);
      ops.push_back(term);
      continue;
    }

    if (peek().kind == Token::Kind::ObjectVariable) {
      if (!parse_object_variable(term))
        return false;
      ops.push_back(term);
      continue;
    }

    if (peek().kind == Token::Kind::BSlash) {
      if (peek(1).kind == Token::Kind::TypeVariable) {
        if (!parse_type_abstraction(term))
          return false;
        ops.push_back(term);
        continue;
      } else if (peek(1).kind == Token::Kind::ObjectVariable) {
        if (!parse_abstraction(term))
          return false;
        ops.push_back(term);
        continue;
      } else
        return false;
    }

    if (parse_term(term)) {
      ops.push_back(term);
      continue;
    }

    return false;
  }

  if (std::holds_alternative<Type>(ops[0]) && ops.size() == 1) {
    t = ops[0];
  }

  if (std::holds_alternative<Type>(ops[0]))
    return false;

  Term ret = std::get<Term>(ops[0]);
  for (size_t i = 1; i < ops.size(); i++) {
    if (std::holds_alternative<Type>(ops[i])) {
      auto ret_i = new TypeApplication;
      ret_i->left = ret;
      ret_i->right = std::get<Type>(ops[i]);
      ret = ret_i;
    } else {
      auto ret_i = new Application;
      ret_i->left = ret;
      ret_i->right = std::get<Term>(ops[i]);
      ret = ret_i;
    }
  }

  t = ret;

  return true;
}
