#pragma once
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <variant>

struct TypeVariable;
struct TypeFunctional;
struct TypeBinder;

using Type = std::variant<TypeVariable *, TypeFunctional *, TypeBinder *>;

struct TypeVariable {
  std::string name;

  bool operator==(TypeVariable const &o) const { return this->name == o.name; }

  bool operator<(TypeVariable const &o) const { return this->name < o.name; }
};

struct TypeFunctional {
  Type from;
  Type to;
};

struct TypeBinder {
  TypeVariable *v;
  Type type;
};

struct ObjectVariable;
struct Application;
struct TypeApplication;
struct Abstraction;
struct TypeAbstraction;

using Term = std::variant<ObjectVariable *, Application *, TypeApplication *,
                          Abstraction *, TypeAbstraction *>;

struct ObjectVariable {
  std::string name;

  bool operator==(ObjectVariable const &o) const {
    return this->name == o.name;
  }
  bool operator<(ObjectVariable const &o) const { return this->name < o.name; }
};

struct Application {
  Term left;
  Term right;
};

struct TypeApplication {
  Term left;
  Type right;
};

struct Abstraction {
  ObjectVariable *v;
  Type type;
  Term term;
};

struct TypeAbstraction {
  TypeVariable *v;
  Term term;
};

using Variable = std::variant<TypeVariable, ObjectVariable>;

struct Context {
  std::set<Variable> domain;
  std::map<ObjectVariable, Type> type;
};

using Op = std::variant<Term, Type>;

void print(Type t);
void print(Term t);
