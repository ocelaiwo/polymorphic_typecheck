#pragma once
#include <string>

class Checker {
public:
  Checker(const std::string &ctx, const std::string &term,
          const std::string &type)
      : ctx_str(ctx), term_str(term), type_str(type) {}

  bool check();

private:
  const std::string &ctx_str;
  const std::string &term_str;
  const std::string &type_str;
};
