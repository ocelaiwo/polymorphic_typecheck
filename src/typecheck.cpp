#include <typecheck.hpp>
#include <utility.hpp>

// Вывод типа согласно правилам вывода λ2.
bool TypeChecker::get_type(Term t, Type &type) {
  if (std::holds_alternative<ObjectVariable *>(t)) {
    auto ov = std::get<ObjectVariable *>(t);
    if (ctx.type.count(*ov) || bound.count(*ov)) {
      if (ctx.type.count(*ov))
        type = ctx.type[*ov];
      else
        type = bound[*ov];
      return true;
    } else
      return false;
  } else if (std::holds_alternative<Application *>(t)) {
    auto a = std::get<Application *>(t);
    Type lt, rt;
    if (!get_type(a->left, lt))
      return false;
    if (!get_type(a->right, rt))
      return false;
    if (std::holds_alternative<TypeFunctional *>(lt)) {
      auto ltf = std::get<TypeFunctional *>(lt);
      if (equivalent(ltf->from, rt)) {
        type = ltf->to;
        return true;
      } else
        return false;
    } else
      return false;
  } else if (std::holds_alternative<TypeApplication *>(t)) {
    auto a = std::get<TypeApplication *>(t);
    Type lt;
    if (!get_type(a->left, lt))
      return false;
    if (!std::holds_alternative<TypeBinder *>(lt))
      return false;
    auto ltb = std::get<TypeBinder *>(lt);
    if (!type_context_check(a->right))
      return false;
    type = type_substitute(ltb->type, ltb->v, a->right);
    return true;
  } else if (std::holds_alternative<Abstraction *>(t)) {
    auto a = std::get<Abstraction *>(t);
    bound[*a->v] = a->type;
    Type t;
    if (!get_type(a->term, t))
      return false;
    auto ret = new TypeFunctional;
    ret->from = a->type;
    ret->to = t;
    type = ret;
    bound.erase(*a->v);
    return true;
  } else if (std::holds_alternative<TypeAbstraction *>(t)) {
    auto a = std::get<TypeAbstraction *>(t);
    Type tt;
    if (ctx.domain.count(*a->v) || domain.count(*a->v))
      return false;
    domain.insert(*a->v);
    if (!get_type(a->term, tt))
      return false;
    domain.erase(*a->v);
    auto tb = new TypeBinder;
    tb->v = a->v;
    tb->type = tt;
    type = tb;
    return true;
  } else
    return false;
}

// Проверка α-конверсии для типов.
bool TypeChecker::equivalent(Type a, Type b) {
  if (std::holds_alternative<TypeVariable *>(a)) {
    if (!std::holds_alternative<TypeVariable *>(b))
      return false;
    auto av = std::get<TypeVariable *>(a);
    auto bv = std::get<TypeVariable *>(b);
    if (ctx.domain.count(*av) && *av == *bv)
      return true;
    if (domain.count(*av) && *av == *bv)
      return true;
    if (binding_vars.count(std::make_pair(*av, *bv)))
      return true;
    else
      return false;
  } else if (std::holds_alternative<TypeFunctional *>(a)) {
    if (!std::holds_alternative<TypeFunctional *>(b))
      return false;
    auto af = std::get<TypeFunctional *>(a);
    auto bf = std::get<TypeFunctional *>(b);
    return (equivalent(af->from, bf->from) && equivalent(af->to, bf->to));
  } else if (std::holds_alternative<TypeBinder *>(a)) {
    if (!std::holds_alternative<TypeBinder *>(b))
      return false;
    auto ab = std::get<TypeBinder *>(a);
    auto bb = std::get<TypeBinder *>(b);
    binding_vars.insert(std::make_pair(*ab->v, *bb->v));
    auto ret = equivalent(ab->type, bb->type);
    binding_vars.erase(std::make_pair(*ab->v, *bb->v));
    return ret;
  } else
    return false;
}

Type TypeChecker::type_substitute(Type type, TypeVariable *o, Type n) {
  if (std::holds_alternative<TypeVariable *>(type)) {
    auto type_v = std::get<TypeVariable *>(type);
    if (type_v->name == o->name)
      return n;
    else
      return type_v;
  } else if (std::holds_alternative<TypeFunctional *>(type)) {
    auto type_f = std::get<TypeFunctional *>(type);
    auto ret = new TypeFunctional;
    ret->from = type_substitute(type_f->from, o, n);
    ret->to = type_substitute(type_f->to, o, n);
    return ret;
  } else if (std::holds_alternative<TypeBinder *>(type)) {
    auto type_b = std::get<TypeBinder *>(type);
    auto ret = new TypeBinder;
    ret->v = type_b->v;
    ret->type = type_substitute(type_b->type, o, n);
    return ret;
  } else
    return (TypeVariable *)nullptr;
}

bool TypeChecker::type_context_check(Type t) {
  if (std::holds_alternative<TypeVariable *>(t)) {
    TypeVariable *tv = std::get<TypeVariable *>(t);
    if (!domain.count(*tv) && !ctx.domain.count(*tv))
      return false;
    else
      return true;
  } else if (std::holds_alternative<TypeFunctional *>(t)) {
    TypeFunctional *tf = std::get<TypeFunctional *>(t);
    return type_context_check(tf->from) && type_context_check(tf->to);
  } else {
    TypeBinder *tb = std::get<TypeBinder *>(t);
    if (ctx.domain.count(*(tb->v)) || domain.count(*(tb->v)))
      return false;
    domain.insert(*(tb->v));
    bool result = type_context_check(tb->type);
    domain.erase(*(tb->v));
    return result;
  }
}

bool TypeChecker::check() {
  Type t;
  if (!get_type(term, t)) {
    std::cerr << "Error during term typechecking." << std::endl;
    return false;
  }
  auto ret = equivalent(t, type);
  std::cout << "Typechecking complete. Suggested type is "
            << (ret ? "correct." : "incorrect.") << std::endl;
  std::cout << "Derived type: ";
  print(t);
  std::cout << std::endl;
  std::cout << "Suggested type: ";
  print(type);
  std::cout << std::endl;
  return ret;
}
