#include <iostream>
#include <string>

#include <lexer.hpp>
#include <parser.hpp>
#include <token.hpp>
#include <typecheck.hpp>

#include <checker.hpp>

bool Checker::check() {
  Lexer ctx_l(ctx_str), term_l(term_str), type_l(type_str);
  std::vector<Token> ctx_t, term_t, type_t;

  if (!ctx_l.lex(ctx_t)) {
    std::cerr << "Error during context lexing" << std::endl;
    return false;
  }
  if (!term_l.lex(term_t)) {
    std::cerr << "Error during term lexing" << std::endl;
    return false;
  }
  if (!type_l.lex(type_t)) {
    std::cerr << "Error during type lexing" << std::endl;
    return false;
  }

  Context ctx;
  Term term;
  Type type;

  Parser p;

  p.load(ctx_t);
  if (!p.parse_context(ctx)) {
    std::cerr << "Error during context parsing" << std::endl;
    return false;
  }
  p.load(term_t);
  if (!p.parse_term(term)) {
    std::cerr << "Error during term parsing" << std::endl;
    return false;
  }
  p.load(type_t);
  if (!p.parse_type(type)) {
    std::cerr << "Error during type parsing" << std::endl;
    return false;
  }
  TypeChecker check(ctx, term, type);
  return check.check();
}
