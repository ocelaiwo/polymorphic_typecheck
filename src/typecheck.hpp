#pragma once
#include <utility.hpp>

class TypeChecker {
public:
  TypeChecker(Context ctx, Term term, Type type)
      : ctx(ctx), term(term), type(type) {}

  bool check();

private:
  Context ctx;
  Term term;
  Type type;

  std::map<ObjectVariable, Type> bound;
  std::set<TypeVariable> domain;

  std::set<std::pair<TypeVariable, TypeVariable>> binding_vars;

private:
  bool get_type(Term t, Type &type);
  bool type_context_check(Type t);
  bool equivalent(Type a, Type b);
  Type type_substitute(Type type, TypeVariable *o, Type n);
};
