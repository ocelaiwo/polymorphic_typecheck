#pragma once
#include <string>

struct Token {
  enum class Kind {
    Empty,
    End,

    ObjectVariable,
    TypeVariable,

    LParen,
    RParen,
    Arrow,
    Colon,
    Comma,
    Star,
    Dot,
    BSlash, // Lambda
    Exmark, // Pi
  };

  enum class Status {

    Error,
    OK
  };

  std::string symbol;
  Kind kind;
  Status status;

  Token(Kind kind, std::string symbol = "")
      : symbol(symbol), kind(kind), status(Status::OK) {}

  Token(std::string error)
      : symbol(error), kind(Kind::Empty), status(Status::Error) {}
};
