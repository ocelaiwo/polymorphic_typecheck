#include <utility.hpp>

void print(Type t) {
  if (std::holds_alternative<TypeVariable *>(t)) {
    TypeVariable *tv = std::get<TypeVariable *>(t);
    std::cout << tv->name;
  } else if (std::holds_alternative<TypeFunctional *>(t)) {
    TypeFunctional *tf = std::get<TypeFunctional *>(t);
    std::cout << "(";
    print(tf->from);
    std::cout << " -> ";
    print(tf->to);
    std::cout << ")";
  } else if (std::holds_alternative<TypeBinder *>(t)) {
    TypeBinder *tb = std::get<TypeBinder *>(t);
    std::cout << "(!";
    std::cout << tb->v->name;
    std::cout << " :* . ";
    print(tb->type);
    std::cout << ")";
  }
}

void print(Term t) {
  if (std::holds_alternative<ObjectVariable *>(t)) {
    auto ov = std::get<ObjectVariable *>(t);
    std::cout << ov->name;
  } else if (std::holds_alternative<Application *>(t)) {
    auto a = std::get<Application *>(t);
    std::cout << "(";
    print(a->left);
    std::cout << " ";
    print(a->right);
    std::cout << ")";
  } else if (std::holds_alternative<TypeApplication *>(t)) {
    auto ta = std::get<TypeApplication *>(t);
    std::cout << "(";
    print(ta->left);
    std::cout << " ";
    print(ta->right);
    std::cout << ")";
  } else if (std::holds_alternative<Abstraction *>(t)) {
    auto a = std::get<Abstraction *>(t);
    std::cout << "(\\";
    std::cout << a->v->name;
    std::cout << " : ";
    print(a->type);
    std::cout << " . ";
    print(a->term);
    std::cout << ")";
  } else {
    auto ta = std::get<TypeAbstraction *>(t);
    std::cout << "(\\";
    std::cout << ta->v->name;
    std::cout << " :* . ";
    print(ta->term);
    std::cout << ")";
  }
}
