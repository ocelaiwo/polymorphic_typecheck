#include <iostream>
#include <vector>

#include <lexer.hpp>
#include <token.hpp>

bool Lexer::lex(std::vector<Token> &result) {
  while (true) {
    auto token = lex_token();
    if (token.status == Token::Status::Error) {
      return false;
    }
    if (token.kind == Token::Kind::End) {
      return true;
    }
    result.push_back(token);
  }
}

int Lexer::peek(size_t n) {
  if (pos + n >= source.size()) {
    return -1;
  } else {
    return source[pos + n];
  }
}

void Lexer::advance(size_t n) { pos += n; }

int Lexer::consume() {
  int ret = peek();
  advance(1);
  return ret;
}

void Lexer::skip_whitespace() {
  while (std::isspace(peek())) {
    advance(1);
  }
}

Token Lexer::lex_typevar() {
  std::string symbol;
  while (std::isalnum(peek())) {
    symbol += consume();
  }
  return Token(Token::Kind::TypeVariable, symbol);
}

Token Lexer::lex_objvar() {
  std::string symbol;
  while (std::isalnum(peek())) {
    symbol += consume();
  }
  return Token(Token::Kind::ObjectVariable, symbol);
}

Token Lexer::lex_regular(Token::Kind k, size_t length) {
  advance(length);
  return Token(k);
}

Token Lexer::lex_token() {
  skip_whitespace();
  int cur = peek();
  if (cur == -1)
    return Token(Token::Kind::End);
  if (std::isupper(cur))
    return lex_typevar();
  if (std::islower(cur))
    return lex_objvar();
  if (cur == '(')
    return lex_regular(Token::Kind::LParen, 1);
  if (cur == ')')
    return lex_regular(Token::Kind::RParen, 1);
  if (cur == '-' && peek(1) == '>')
    return lex_regular(Token::Kind::Arrow, 2);
  if (cur == ':')
    return lex_regular(Token::Kind::Colon, 1);
  if (cur == ',')
    return lex_regular(Token::Kind::Comma, 1);
  if (cur == '*')
    return lex_regular(Token::Kind::Star, 1);
  if (cur == '.')
    return lex_regular(Token::Kind::Dot, 1);
  if (cur == '\\')
    return lex_regular(Token::Kind::BSlash, 1);
  if (cur == '!')
    return lex_regular(Token::Kind::Exmark, 1);
  return Token("Lexing error.");
}
