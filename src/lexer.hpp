#pragma once
#include <string>
#include <vector>

#include <token.hpp>

// Просто лексер для удобства работы.
class Lexer {
public:
  Lexer(const std::string &source) : source(source), pos(0) {}

  bool lex(std::vector<Token> &result);

private:
  int peek(size_t n = 0);
  void advance(size_t n);
  int consume();
  void skip_whitespace();

  Token lex_typevar();
  Token lex_objvar();
  Token lex_regular(Token::Kind k, size_t length);

  Token lex_token();

private:
  const std::string &source;
  size_t pos;
};
