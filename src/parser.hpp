#pragma once
#include <vector>

#include <token.hpp>
#include <utility.hpp>

class Parser {
public:
  void load(std::vector<Token> tokens);
  bool parse_context(Context &ctx);
  bool parse_term(Term &t);
  bool parse_type(Type &t);

private:
  std::vector<Token> tokens;
  size_t pos;

  // Для проверки корректности контекста.
  std::set<Variable> bound;

  // Для соблюдения скобочного баланса в выражениях.
  int type_parens = 0;
  int parens = 0;

private:
  Token peek(size_t n = 0);
  void advance(size_t n);

  bool type_context_check(const Type &t, const Context &ctx);

  bool parse_type_variable(Type &t);
  bool parse_type_binder(Type &t);
  bool parse_object_variable(Term &t);
  bool parse_abstraction(Term &t);
  bool parse_type_abstraction(Term &t);
  bool parse_term_internal(Op &t);
};
