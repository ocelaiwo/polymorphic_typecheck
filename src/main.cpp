#include <iostream>
#include <string>

#include <checker.hpp>

int main() {
  std::string ctx_str, term_str, type_str;
  std::cout << "Enter context: ";
  std::getline(std::cin, ctx_str);
  std::cout << "Enter term: ";
  std::getline(std::cin, term_str);
  std::cout << "Enter type: ";
  std::getline(std::cin, type_str);

  Checker check(ctx_str, term_str, type_str);
  check.check();
  return 0;
}
