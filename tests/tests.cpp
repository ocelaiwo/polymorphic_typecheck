#include "gtest/gtest.h"

#include <string>
#include <vector>

#include <checker.hpp>


TEST(Suite, A) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "A:*, a:A";
  term_str = "a";
  type_str = "A";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, B) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "A:*, B:*, a:A->B, b:A";
  term_str = "a b";
  type_str = "B";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, C) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "A:*,B:*, f: !C:* . C -> A, x : B";
  term_str = "f B x";
  type_str = "A";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, D) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "x : !V:*. V";
  term_str = "x";
  type_str = "!V:*. V";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, E) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "";
  term_str = "\\V:* . \\v : V . v";
  type_str = "!V:* . V -> V";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}


TEST(Suite, F) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "";
  term_str = "\\A:* . \\f : A -> A . \\x : A . f (f x)";
  type_str = "!A:* . (A -> A) -> A -> A";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, G) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "Nat:*, Bool:*";
  term_str = "(\\A : * . \\B : * . \\f : A -> A . \\g : A -> B . \\x : A . g(f(f x))) Nat Bool";
  type_str = "(Nat -> Nat) -> (Nat -> Bool) -> (Nat -> Bool)";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, H) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "";
  term_str = "\\x: !A:*.A . x ((!A:*.A) -> (!A:*.A) -> (!A:*.A)) (x((!A:*.A) -> (!A:*.A))x)(x((!A:*.A) -> (!A:*.A) -> (!A:*.A))x x)";
  type_str = "(!A :* . A) -> (!A :* . A)";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}

TEST(Suite, I) {
  std::string ctx_str, term_str, type_str;
  ctx_str = "";
  term_str = "\\x: !A:*.A . x";
  type_str = "(!B :* . B) -> (!A :* . A)";

  Checker check(ctx_str, term_str, type_str);
  ASSERT_EQ(check.check(), true);
}



int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
