# Typechecker (λ2)
## Сборка
Используется система сборки [Meson](https://mesonbuild.com/). Для сборки
проекта перейдите в корень проекта и исполните:
```
meson setup build
cd build
meson compile
```
## Тестирование
В директории сборки исполните:
```
meson test --verbose
```
## Запуск
Формат согласно книге "Type Theory and Formal Proof", переменные типов начинаются с заглавной буквы, объектов - строчной, переменные отделяются пробелом,
вместо `λ` используется `\`, вместо `Π` - `!`. Контекст записывается через запятую.

Для запуска перейдите в `build/src` и запустите `typecheck`.

